// This executes a matrix-vector-multiplication of a NxN matrix with a vector of size N
// Compile with: gcc -O2 -o 0428a 0428a.c

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

void matrix_vector_mul(int N, double *A, double *x, double *y) {
    int irow;
    int icolumn;
    register double accumulator;

    #if 0
    for(irow = 0; irow < N; irow++) {
        y[irow] = 0;
        for(icolumn = 0; icolumn < N; icolumn++) {
            y[irow] += A[irow*N + icolumn] * x[icolumn];
        }
    }
    #else
    for(irow = 0; irow < N; irow++) {
        accumulator = 0;
        for(icolumn = 0; icolumn < N; icolumn++) {
            accumulator += (*A) * x[icolumn];
            A++;
        }
        y[irow] = accumulator;
    }
    #endif
}

int main(int argc, char *argv[]) {
    double *A;
    double *x;
    double *y;
    int N;
    int M;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;

    if(argc == 3) {
        N = atoi(argv[1]);
        M = atoi(argv[2]);
    }
    else {
        printf("Usage: 0428a <N> <M>\n");
        return -1;
    }
    printf("N = %d, M = %d\n", N, M);

    A = malloc(sizeof(double) * N * N);
    x = malloc(sizeof(double) * N);
    y = malloc(sizeof(double) * N);

    for(i = 0; i < N * N; i++) {
        A[i] = i + 1;
    }
    for(i = 0; i < N; i++) {
        x[i] = i + 1;
    }

    sleep(1);  // to finish mem initializations

    gettimeofday(&t1, NULL);
    for(i = 0; i < M; i++) {
        #if 1
        for(irow = 0; irow < N; irow++) {
            y[irow] = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                y[irow] += A[irow*N + icolumn] * x[icolumn];
            }
        }
        #else
        matrix_vector_mul(N, A, x, y);
        #endif
    }
    gettimeofday(&t2, NULL);
    duration_us = my_usecdiff(&t1, &t2);
    printf("Operation took %d us -> %d ops/us\n", duration_us, (int)((uint64_t)(N) * N * M / duration_us));
    
    if(N <= 3) {
        printf("y = ");
        for(i = 0; i < N; i++) {
            printf("%lf ", y[i]);
        }
        printf("\n");
    }

    free(A);
    free(x);
    free(y);

    return 0;
}