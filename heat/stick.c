/*
 * Calculates the heat in a stick, which is heated at one end with 100 degrees.
 * It stops when the temperature at target_x is greater than 45 degrees.
 * 
 * OLD version, please use stick_gnuplot.c instead
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define N_POINTS 1000

int main(int argc, char *argv[])
{
    // parameters
    // double density = 7.8e3; // kg / m^3
    // double lambda = 15;     // W / (m * K)
    // double specific_heat_capacity = 0.5e3;  // J / (kg * K)
    // double alpha = lambda / (density * specific_heat_capacity);
    double alpha = 110e-6; // 3.8e-6; // m^2 / s
    double L = 1;
    double T_max = 45;
    double target_x = 0.8;
    double T_start = 20;
    double h = L / N_POINTS;
    double tau = h*h / (2*alpha) * 0.1;
    double T_heating = 100;
    int i_iteration = 0;

    double u[N_POINTS];
    double u_new[N_POINTS];
    int i;
    double current_t = 0;
    int target_i = round(target_x / h);

    char *filename = "stick1.csv";
    FILE *output_csv_file;

    if(argc == 2) {
        filename = argv[1];
    }
    printf("Using output file %s\n", filename);
    output_csv_file = fopen(filename, "w+");
    if(output_csv_file == NULL) {
        perror("Could not open output csv file");
        return -2;
    }

    printf("h = %lf, tau = %le, alpha = %lf mm^2/s %i\n", h, tau, alpha * 1e6, target_i);

    for(i = 1; i < N_POINTS; i++) {
        u[i] = T_start;
    }
    u[0] = T_heating;
    u_new[0] = T_heating;

    for(i = 0; i < N_POINTS; i++) {
        fprintf(output_csv_file, "%lf;", u[i]);
    }
    fprintf(output_csv_file, "\n");

    while(u[target_i] < T_max) {
        #pragma omp parallel
        #pragma omp for private(i)
        for(i = 1; i < N_POINTS - 1; i++) {
            u_new[i] = u[i] + alpha * tau / (h*h) * (u[i+1] + u[i-1] - 2*u[i]);
        }
        
        u_new[N_POINTS - 1] = u_new[N_POINTS - 2];

        memcpy(u, u_new, sizeof(double) * N_POINTS);  // overwrite u with the new values

        i_iteration++;
        current_t = i_iteration * tau;

        if(i_iteration % 100 == 0) {
            printf("i_iteration=%7i, t=%9lf, u(0.8m)=%9lf, %9lf %9lf %9lf\r", i_iteration, current_t, u[target_i], u_new[N_POINTS - 3], u_new[N_POINTS - 2], u_new[N_POINTS - 1]);
        }
        if(i_iteration % 20000 == 0) {
            for(i = 0; i < N_POINTS; i++) {
                fprintf(output_csv_file, "%lf;", u[i]);
            }
            fprintf(output_csv_file, "\n");
        }

        if(i_iteration > 10000000) {
            printf("Stopping because it takes to long\n");
            break;
        }
    }
    printf("\nreached %lf *C on t = %lf s\n", u[target_i], current_t);
    fprintf(output_csv_file, "%lf\n", current_t);

    fclose(output_csv_file);

    return 0;
}