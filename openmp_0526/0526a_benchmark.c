/*
 * Runs a matrix vector multiplication of an NxN-matrix with a vector of size N on 4 cores using MPI.
 * This is done by letting each process compute the result for a part of the lines of the matrix.
 * Benchmarks this by using different N sizes.
 * M is kept constant across the benchmark.
 * 
 * This file is intended to be used in conjunction with 0526a_benchmark.py.
 * 
 * Compile with: gcc -fopenmp -O2 -o 0526a_benchmark 0526a_benchmark.c
 * put -g after gcc for debugging
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#define NUM_PASSES 10

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

// N: problem size, M: number of times the problem is repeated
// Returns the number of usec of the whole iteration (-> not divided by M)
int do_benchmark(int N, int M) {
    double *A;
    double *x;
    double *y;
    double *A_tmp;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;
    register double accumulator;
    int problem_iteration;

    A = malloc(sizeof(double) * N * N);
    x = malloc(sizeof(double) * N);
    y = malloc(sizeof(double) * N);

    for(i = 0; i < N * N; i++) {
        A[i] = i + 1;
    }
    for(i = 0; i < N; i++) {
        x[i] = i + 1;
    }

    gettimeofday(&t1, NULL);
    for(problem_iteration = 0; problem_iteration < M; problem_iteration++) {
        #ifdef INNER_LOOP

        // should be slower because of more overhead
        for(irow = 0; irow < N; irow++) {
            accumulator = 0;

            #pragma omp parallel for reduction (+ : accumulator)
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }

            y[irow] = accumulator;
        }

        #else
        #pragma omp parallel private(irow, accumulator, icolumn)
        #pragma omp for
        for(irow = 0; irow < N; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        #endif
    }
    gettimeofday(&t2, NULL);

    duration_us = my_usecdiff(&t1, &t2);
    printf("Operation took %d us incl. send and receive -> effectively %ld MFLOPS\n", 
            duration_us / M, (uint64_t)N * N * M / duration_us * 2);

    if(N <= 4) {
        printf("y = ");
        for(i = 0; i < N; i++) {
            printf("%lf ", y[i]);
        }
        printf("\n");
    }

    free(A);
    free(x);
    free(y);

    return duration_us;
}

int main(int argc, char *argv[]) {
    const char *filename;
    FILE *output_csv_file;
    double N_with_comma;
    uint64_t duration_us;
    int M;
    int N;
    int i;
    int num_passes;

    if(argc == 2) {
        filename = argv[1];
    }
    else {
        printf("Usage: 0526a_benchamrk <output_csv_filename>\n");
        return -1;
    }

    printf("sizeof(int) = %ld, sizeof(void*) = %ld, CLOCKS_PER_SEC = %ld, max_threads = %d\n", sizeof(int), sizeof(void*), CLOCKS_PER_SEC, omp_get_max_threads());

    output_csv_file = fopen(filename, "w+");
    if(output_csv_file == NULL) {
        perror("Could not open output csv file");
        return -2;
    }

    for(N_with_comma = 4; N_with_comma < 10000; N_with_comma *= 1.4142135623730950488) {  // sqrt(2)
        N = ((int)N_with_comma / 4) * 4;  // just to be consistent with the MPI implementations

        M = 500;
        if(N < 1000)
            num_passes = NUM_PASSES;
        else
            num_passes = 10;
        
        duration_us = 0;
        for(i = 0; i < num_passes; i++) {
            duration_us += do_benchmark(N, M);
            //usleep(100000);  // cooldown, allow other processes to enter
        }

        fprintf(output_csv_file, "%d;%d;%lf\n", N, M, (double)duration_us / num_passes);
    }

    fclose(output_csv_file);

    return 0;
}
