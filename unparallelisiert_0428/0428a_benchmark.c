/*
 * This file is intended to be compiled and executed with 0428a_benchmark.py
 * compile with: e.g. gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DUSE_ACCUMULATOR -DUSE_FUNCTION_FOR_MUL
 * put -g after gcc for debugging
 * TODO evtl noch mal mit pointer pruefen
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>

#define NUM_PASSES 50

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

//static inline 
void matrix_vector_mul(int N, double *A, double *x, double *y) {
    int irow;
    int icolumn;
    register double accumulator;

    #ifdef USE_ACCUMULATOR
    #warning Info: using accumulator
    for(irow = 0; irow < N; irow++) {
        accumulator = 0;
        for(icolumn = 0; icolumn < N; icolumn++) {
            accumulator += (*A) * x[icolumn];
            A++;
        }
        y[irow] = accumulator;
    }
    #else
    #warning Info: not using accumulator
    for(irow = 0; irow < N; irow++) {
        y[irow] = 0;
        for(icolumn = 0; icolumn < N; icolumn++) {
            y[irow] += A[irow*N + icolumn] * x[icolumn];
        }
    }
    #endif
}

// N: problem size, M: number of times the problem is repeated
int do_benchmark(int N, int M) {
    double *A;
    double *x;
    double *y;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;
    #ifdef USE_ACCUMULATOR
    register double accumulator;
    #endif

    A = malloc(sizeof(double) * N * N);
    x = malloc(sizeof(double) * N);
    y = malloc(sizeof(double) * N);

    for(i = 0; i < N * N; i++) {
        A[i] = i + 1;
    }
    for(i = 0; i < N; i++) {
        x[i] = i + 1;
    }

    usleep(10000); //sleep(1);  // to finish mem initializations

    gettimeofday(&t1, NULL);
    for(i = 0; i < M; i++) {
        #ifdef USE_FUNCTION_FOR_MUL
        #warning Info: using function for mul
        matrix_vector_mul(N, A, x, y);
        #else
        #warning Info: using inline instead of function for mul
        #ifdef USE_ACCUMULATOR
        #warning Info: not using accumulator
        for(irow = 0; irow < N; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += (*A) * x[icolumn];
                A++;
            }
            y[irow] = accumulator;
        }
        #else
        #warning Info: using accumulator
        for(irow = 0; irow < N; irow++) {
            y[irow] = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                y[irow] += A[irow*N + icolumn] * x[icolumn];
            }
        }
        #endif
        #endif
    }
    gettimeofday(&t2, NULL);
    duration_us = my_usecdiff(&t1, &t2);
    if(duration_us == 0)
        printf("N=%d, M=%d => 0 us\n", N, M);
    else
        printf("N=%d, M=%d => %d us -> %d ops/us\n", N, M, duration_us, (int)((uint64_t)(N) * N * M / duration_us));

    if(N <= 3) {
        printf("y = ");
        for(i = 0; i < N; i++) {
            printf("%lf ", y[i]);
        }
        printf("\n");
    }

    free(A);
    free(x);
    free(y);

    return duration_us;
}

int main(int argc, char *argv[]) {
    const char *filename;
    FILE *output_csv_file;
    double N_with_comma;
    int duration_us;
    int M;
    int i;
    int num_passes;

    if(argc == 2) {
        filename = argv[1];
    }
    else {
        printf("Usage: 0428a_benchamrk <output_csv_filename>\n");
        return -1;
    }
    output_csv_file = fopen(filename, "w+");
    if(output_csv_file == NULL) {
        perror("Could not open output csv file");
        return -2;
    }

    #ifdef SAME_M
    for(N_with_comma = 2; N_with_comma < 5000; N_with_comma *= 1.4142135623730950488) {  // sqrt(2)
        M = 500;
        if(N_with_comma < 1000)
            num_passes = NUM_PASSES;
        else
            num_passes = 10;
        
        duration_us = 0;
        for(i = 0; i < num_passes; i++) {
            duration_us += do_benchmark(N_with_comma, M);
            usleep(100000);  // cooldown, allow other processes to enter
        }

        fprintf(output_csv_file, "%d;%d;%lf\n", (int)N_with_comma, M, (double)duration_us / num_passes);
    }
    #else
    // 32768 is approx. 8 GiB of RAM
    for(N_with_comma = 2; N_with_comma < 30000; N_with_comma *= 1.4142135623730950488) {  // sqrt(2)
        // if(N_with_comma < 100)
        //     M = 100000;
        // else if(N_with_comma < 5000)
        //     M = 200;
        // else
        //     M = 5;
        M = 10000000 / ((int)N_with_comma * (int)N_with_comma) + 3;
        if(N_with_comma < 4000)
            num_passes = NUM_PASSES;
        else
            num_passes = 10;
        
        duration_us = 0;
        for(i = 0; i < num_passes; i++) {
            duration_us += do_benchmark(N_with_comma, M);
            usleep(100000);  // cooldown, allow other processes to enter
        }

        fprintf(output_csv_file, "%d;%d;%lf\n", (int)N_with_comma, M, (double)duration_us / num_passes);
    }
    #endif

    fclose(output_csv_file);

    return 0;
}