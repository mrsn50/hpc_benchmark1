# This will use the same number of operations (-> M) for all problem sizes

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

if (len(sys.argv) > 1) and (sys.argv[1] == 'exec'):
    # taskset -c <list of CPUs to use>: this force OpenMP to use the
    # same amount of threads as MPI (for better comparison)
    script = """
    gcc -fopenmp -O2 -o 0526a_benchmarking 0526a_benchmark.c
    ./0526a_benchmarking 0526a.csv
    gcc -fopenmp -O2 -o 0526a_benchmarking 0526a_benchmark.c
    taskset -c 1-4 ./0526a_benchmarking 0526a_4t.csv
    gcc -fopenmp -O2 -o 0526a_benchmarking 0526a_benchmark.c -DINNER_LOOP
    taskset -c 1-4 ./0526a_benchmarking 0526a_innerloop_4t.csv
    """
    for line in script.split('\n'):
        print(line)
        if os.system(line) != 0:
            print("error running:", line)
            sys.exit(-1)

def benchmark(filename, label=None):
    if label is None:
        label = filename[:-4]
    # strip because '\n' on the end
    values = [[float(val) for val in line.strip().split(';')] for line in open(filename).readlines()]
    values = np.array(values)
    if values.shape[0] > 0:
        plt.plot(values[:,0], (values[:,1] * values[:,0] * values[:,0]) / values[:,2], '-x', label=label)
    plt.xlabel("N")
    plt.ylabel("ops / us")
    plt.xscale("log")
benchmark("0526a_4t.csv", "outer loop taskset 4")
benchmark("0526a_innerloop_4t.csv", "inner loop taskset 4")
benchmark("../mpi_naive_0505/0505a.csv", "MPI")
benchmark("../unparallelisiert_0428/acc_mulfunc_samem.csv", "unparallelized")
benchmark("0526a.csv", "outer loop 8")
plt.legend()
plt.title("same M")
plt.grid('both')
plt.show()