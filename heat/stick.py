import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import itertools
import sched, time

values = [[float(val) for val in line.strip()[:-1].split(';')] for line in open("stick1.csv").readlines()]
t_end = values[-1][0]  # last line is only a single value: the time of the last sample
values = np.array(values[:-1])
# plt.pcolor([values[-1]], cmap='plasma')
# plt.show()

# Using matplotlib animation works but does not display any slider
# (but https://matplotlib.org/stable/gallery/animation/bayes_update.html#sphx-glr-gallery-animation-bayes-update-py shows a slider - I dont know I have the most current matplotlib version but maybe there are linux problems)

# def data_gen():
#     for cnt in itertools.count():
#         t = cnt / t_end
#         yield t, values[cnt % values.shape[0]]

# def init():
#     return pcolor,

# fig, ax = plt.subplots()
# pcolor = ax.pcolormesh([values[-1]], cmap='plasma')

# def run(data):
#     # update the data
#     t, y = data
#     pcolor.set_array(np.array(y))
#     return pcolor,

# ani = matplotlib.animation.FuncAnimation(fig, run, data_gen, interval=10, init_func=init, blit=True)
# plt.show()

fig, ax = plt.subplots(2, 1, figsize=(12, 6), gridspec_kw={"height_ratios": [0.7, 0.3]})
x_values = np.linspace(0, 1, values.shape[1], endpoint=False)
line, = ax[0].plot(x_values, values[-1])
pcolor = ax[1].pcolormesh([values[-1]], cmap='plasma', vmin=0, vmax=100)
ax[0].margins(x = 0)
ax[0].grid('both')
ax[0].plot([0, 1], [45, 45])
ax[0].set_ylim(0, 100)
ax[1].margins(x = 0)
ax[0].set_ylabel('Temperature / *C')

# adjust the main plot to make room for the slider
plt.subplots_adjust(bottom=0.25)

current_t = 0

# Make a horizontal slider to control the frequency.
axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.13, 0.1, 0.77, 0.03], facecolor=axcolor)
t_slider = Slider(
    ax=axfreq,
    label='Time / s',
    valmin=0,
    valmax=t_end,
    valinit=t_end,
)

def update(val):
    global current_t
    current_t = val
    step = int(min(val / t_end * values.shape[0], values.shape[0] - 1))
    pcolor.set_array(np.array([values[step]]))
    line.set_data(x_values, values[step])
    # fig.canvas.draw_idle()

t_slider.on_changed(update)

# Create a `matplotlib.widgets.Button to start an automatic animation
start_stop_anim_ax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(start_stop_anim_ax, 'Play / Pause', color=axcolor, hovercolor='0.975')

animation_running = True

def start_stop(event):
    global animation_running
    global current_t
    animation_running = not animation_running
    if animation_running and current_t > t_end - 0.1:
        current_t = 0
button.on_clicked(start_stop)

def timer_func():
    global current_t
    global animation_running
    
    if(animation_running):
        new_t = current_t + t_end / 100
        if new_t > t_end:
            animation_running = 0
        t_slider.set_val(new_t)
    plt.pause(0.001)

    if plt.fignum_exists(fig.number):
        s.enter(0.05, 1, timer_func)

s = sched.scheduler()
s.enter(0.05, 1, timer_func)

# Show the plot, but without blocking updates
plt.pause(0.05)

s.run()

# while(True):
#     timer_func()
#     plt.pause(0.1)