/*
 * Runs a matrix vector multiplication of an NxN-matrix with a vector of size N on 4 cores using MPI.
 * This is done by letting each process compute the result for a part of the lines of the matrix.
 * Compile with: mpicc -O2 -o 0512a 0512a.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <mpi.h>

#define MY_TAG 5678

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

int main(int argc, char *argv[]) {
    double *A;
    double *x;
    double *y;
    double *A_tmp;
    int N;
    int M;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;
    register double accumulator;
    int problem_iteration;
    int itest;

    if(argc == 3) {
        N = atoi(argv[1]);
        M = atoi(argv[2]);
    }
    else {
        printf("Usage: 0428a <N> <M>\nN is the size of the matrix (NxN).\nM is the number of repetitions.\n");
        return -1;
    }

    MPI_Init(&argc, &argv);
    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    if(N % world_size != 0) {
        printf("Error: N has to be a multiple of the processor count (%d)\n", world_size);
        return -1;
    }

    if(world_rank == 0) {
        // master process
        printf("N = %d, M = %d\n", N, M);

        A = malloc(sizeof(double) * N * N);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N);

        for(i = 0; i < N * N; i++) {
            A[i] = i + 1;
        }
        for(i = 0; i < N; i++) {
            x[i] = i + 1;
        }
    }
    else {
        A = malloc(sizeof(double) * N * N / world_size);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N);
    }

    // siehe Fixpunktiteration
    MPI_Scatter(A, N * N / world_size, MPI_DOUBLE, A, N * N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if(world_rank == 0) {
        // the master process does not need the whole matrix anymore, so free it and keep only the first slice
        A_tmp = malloc(sizeof(double) * N * N / world_size);
        memcpy(A_tmp, A, sizeof(double) * N * N / world_size);
        free(A);
        A = A_tmp;
    }

    gettimeofday(&t1, NULL);
    for(problem_iteration = 0; problem_iteration < M; problem_iteration++) {
        MPI_Bcast(x, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        for(irow = 0; irow < N / world_size; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        
        //MPI_Gather(y, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Allgather(y, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    gettimeofday(&t2, NULL);

    if(world_rank == 0) {
        duration_us = my_usecdiff(&t1, &t2);
        printf("Operation took %d us incl. send and receive -> effectively %ld MFLOPS\n", 
                duration_us / M, (uint64_t)N * N * M / duration_us * 2);

        if(N <= 4) {
            printf("y = ");
            for(i = 0; i < N; i++) {
                printf("%lf ", y[i]);
            }
            printf("\n");
        }
    }

    free(A);
    free(x);
    free(y);

    MPI_Finalize();

    return 0;
}