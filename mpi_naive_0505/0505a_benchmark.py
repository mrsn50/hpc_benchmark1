# This will use the same number of operations (-> M) for all problem sizes

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

if (len(sys.argv) > 1) and (sys.argv[1] == 'exec'):
    script = """
    mpicc -O2 -o 0505a_benchmark 0505a_benchmark.c
    mpirun 0505a_benchmark 0505a.csv
    mpicc -O2 -o 0505a_benchmark 0505a_benchmark.c -DDONT_FREE_MATRIX
    mpirun 0505a_benchmark 0505a_dontfree.csv
    """
    for line in script.split('\n'):
        print(line)
        if os.system(line) != 0:
            print("error running:", line)
            sys.exit(-1)

def benchmark(filename):
    # strip because '\n' on the end
    values = [[float(val) for val in line.strip().split(';')] for line in open(filename).readlines()]
    values = np.array(values)
    if values.shape[0] > 0:
        plt.plot(values[:,0], (values[:,1] * values[:,0] * values[:,0]) / values[:,2], '-x')
    plt.xlabel("N")
    plt.ylabel("ops / us")
    plt.xscale("log")
benchmark("0505a.csv")
benchmark("0505a2.csv")
benchmark("0505a_dontfree.csv")
plt.title("same M")
plt.legend(["freeing (pass 1)", "freeing (pass 2)", "no free"])
plt.grid('both')
plt.show()