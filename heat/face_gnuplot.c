/*
 * Calculates the temperature of a face, which is heated at (heating_x, heating_y) with 100 degrees.
 * It stops when the temperature at (target_x, target_y) is greater than 45 degrees.
 *
 * Es fehlt die Beruecksichtigung der Temperaturabhaengigkeit von alpha.
 *
 * Please install feedgnuplot first (for graphics)
 * Compile & run with:
 * gcc -O2 -fopenmp -o face_gnuplot face_gnuplot.c -lm && ./face_gnuplot | feedgnuplot --domain --stream 0 --3d --with 'pm3d' --set 'pm3d corners2color c1' --set 'cbrange [20:100]' --set 'dgrid3d 100,100,1' --set 'view map'
 * Note: the dgrid3d parameter must be adapted like dgrid3d N_YPOINTS,N_XPOINTS,1 (it will also work but interpolate which is very ugly)
 * Remove --set 'view map' to better see which points gnuplot actually displays
 *
 * Note: for this plot type, gnuplot (or formatting the data for gnuplot) measuarbly slows the program down. So please comment out the gnuplot section.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <omp.h>

// also tested for N_XPOINTS != N_YPOINTS
#define N_XPOINTS 100
#define N_YPOINTS 100

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

double my_min(double a, double b) {
    return a < b ? a : b;
}

#ifndef _OPENMP
    #define omp_get_max_threads() (-1)
#endif

int main(int argc, char *argv[])
{
    // parameters
    double alpha = 110e-6; // m^2 / s  // 110e-6 = copper, 3.8e-6 = Edelstahl
    double L_x = 1;
    double L_y = 1;
    double T_max = 45;
    double target_x = 0.8;
    double target_y = 0.8;
    double T_start = 20;
    double h_x = L_x / N_XPOINTS;
    double h_y = L_y / N_YPOINTS;
    double smaller_h = my_min(h_x, h_y);
    double tau = smaller_h*smaller_h / (2*alpha) * 0.01;  // 0.1 is more than enough and would be much faster, but we use 0.01 because we want enough samples to measure the performance
    double T_heating = 100;
    double heating_x = 0.3;
    double heating_y = 0.4;

    struct timeval t1, t2;
    int duration_us;

    int b_continue = 1;

    double u[N_XPOINTS][N_YPOINTS];
    double u_new[N_XPOINTS][N_YPOINTS];
    int i;
    int ix;
    int iy;
    double current_t = 0;
    int target_ix = round(target_x / h_x);
    int target_iy = round(target_y / h_y);
    int heating_ix = round(heating_x / h_x);
    int heating_iy = round(heating_y / h_y);
    int i_iteration = 0;

    char *filename = "stick1.csv";
    FILE *output_csv_file;

    const double at_x = alpha * tau / (h_x*h_x);
    const double at_y = alpha * tau / (h_y*h_y);

    fprintf(stderr, "h_smaller = %lf, tau = %le, alpha = %lf mm^2/s, max_threads=%i, target_i=%i %i\n", smaller_h, tau, alpha * 1e6, omp_get_max_threads(), target_ix, target_iy);

    for(ix = 1; ix < N_XPOINTS; ix++) {
        for(iy = 1; iy < N_YPOINTS; iy++) {
            u[ix][iy] = T_start;
        }
    }
    u[heating_ix][heating_iy] = T_heating;

    gettimeofday(&t1, NULL);

    #pragma omp parallel shared(u_new, u, i_iteration, current_t, b_continue) private(iy)
    while(b_continue) {
        #pragma omp for
        for(ix = 1; ix < N_XPOINTS - 1; ix++) {
            for(iy = 1; iy < N_YPOINTS - 1; iy++) {
                u_new[ix][iy] = u[ix][iy] + at_x * (u[ix+1][iy] + u[ix-1][iy] - 2*u[ix][iy]) + at_y * (u[ix][iy+1] + u[ix][iy-1] - 2*u[ix][iy]);
            }
        }

        // parallelized version of memcpy. For some reason, this resulted in a huge performance gain (x2) compared to running memcpy inside the omp single section
        #pragma omp for
        for(i = 1; i < N_XPOINTS * N_YPOINTS; i++) {
            *((double*)u + i) = *((double*)u_new + i);
        }

        // edges: no heat flux at edges (called "neumann boundary conditions")
        #pragma omp for
        for(i = 1; i < N_XPOINTS; i++) {
            u[i][0] = u[i][1];
            u[i][N_YPOINTS - 1] = u[i][N_YPOINTS - 2];
        }
        #pragma omp for
        for(i = 1; i < N_YPOINTS - 1; i++) {
            u[0][i] = u[1][i];
            u[N_XPOINTS - 1][i] = u[N_XPOINTS - 2][i];
        }

        // omp single: only executed by 1 thread
        // vs. omp critical: executed by all threads, but sequentially
        #pragma omp single
        {
            // peak edges
            u[0][0] = (u[0][1] + u[1][0]) / 2;
            u[0][N_YPOINTS - 1] = (u[0][N_YPOINTS - 2] + u[1][N_YPOINTS - 1]) / 2;
            u[N_XPOINTS - 1][0] = (u[N_XPOINTS - 2][0] + u[N_XPOINTS - 1][1]) / 2;
            u[N_XPOINTS - 1][N_YPOINTS - 1] = (u[N_XPOINTS - 1][N_YPOINTS - 2] + u[N_XPOINTS - 2][N_YPOINTS - 1]) / 2;

            // heat source
            u[heating_ix][heating_iy] = T_heating;

            i_iteration++;
            current_t = i_iteration * tau;

            if(i_iteration % 200000 == 0) {
                fprintf(stderr, "i_iteration=%7i, t=%9lf, u(0.8m)=%9lf, u0=%9lf\r", i_iteration, current_t, u[target_ix][target_iy], u[0][0]);
                // for gnuplot
                // in 3d, --monotonic or auto-replot does not work, so do manual replotting here
                printf("clear\n");
                for(ix = 0; ix < N_XPOINTS; ix++) {
                    for(iy = 0; iy < N_YPOINTS; iy++) {
                        printf("%d %d %lf\n", ix, iy, u[ix][iy]);
                    }
                }
                printf("replot\n");
            }

            if(u[target_ix][target_iy] > T_max) {
                b_continue = 0;
            }
        }
    }

    gettimeofday(&t2, NULL);
    duration_us = my_usecdiff(&t1, &t2);
    fprintf(stderr, "\nreached %lf *C on t = %lf s\n", u[target_ix][target_iy], current_t);
    fprintf(stderr, "computation took %lf seconds -> %lf us per iteration\n", duration_us / 1e6, duration_us / (double)i_iteration);

    return 0;
}