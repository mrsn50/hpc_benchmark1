# hpc_benchmark1

Der Ordner unparallelisiert_0428 bezieht sich auf den unparallelisierten Code zum 28.04 (Benchmarks gezeigt am 05.05). Für Schnelltests ist die Datei 0428a.c gedacht (führt den Code nur für ein N aus, das über Kommandozeilenargument festlegbar ist).

Der Ordner mpi_naive_0505 bezieht sich auf den mit MPI parallelisierten Code zum 05.05 (Benchmarks gezeigt am 12.05). Für Schnelltests ist die Datei 0505a.c gedacht (führt den Code nur für ein N aus, das über Kommandozeilenargument festlegbar ist).

Der Ordner mpi_gather_0512 bezieht sich die Verbesserung des MPI-Codes mit den MPI_Scatter, MPI_Gather usw. Funktionen.

Der Ordner openmp_0526 bezieht sich auf die Realisierung mit OpenMP (benchmarks gezeigt am 01.06). Er enthält auch meine Implementierung zur skalaren Multiplikation in OpenMP.

Der Ordner heat bezieht sich auf die Wärmeleitungsgleichung: Ein 1m langer Stab wird an einem Ende auf 100 *C erhitzt. Der Algorithmus rechnet, bis Die Stelle bei 0.8m 45 *C heiß ist. Die Datei stick_gnuplot.c verwendet dabei OpenMP-Parallelisierung (stick.c ist alt). Dann gibt es noch die Datei face_gnuplot.c, bei der eine 2D Platte an einem Punkt erhitzt wird und ein anderer geprüft wird.