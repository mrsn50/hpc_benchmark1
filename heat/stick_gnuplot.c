/*
 * Calculates the heat in a stick, which is heated at one end with 100 degrees.
 * It stops when the temperature at target_x is greater than 45 degrees.
 *
 * Es fehlt die Beruecksichtigung der Temperaturabhaengigkeit von alpha.
 *
 * Please install feedgnuplot first (for graphics)
 * Compile & run with:
 * gcc -O2 -fopenmp -o stick_gnuplot stick_gnuplot.c -lm && ./stick_gnuplot | feedgnuplot --domain --monotonic --lines --stream --xmin=0 --xmax=999 --ymin 20 --ymax 100
 * or unparallelized for comparison:
 * gcc -O2 -o stick_gnuplot stick_gnuplot.c -lm && ./stick_gnuplot | feedgnuplot --domain --monotonic --lines --stream --xmin=0 --xmax=999 --ymin 20 --ymax 100
 *
 * The gnuplot overhead is neglectible
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

// 1000 -> no performance gain when running parallelized
// 2000 -> performance gain (approx 2x when running on 4 threads)
#define N_POINTS 1000

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

int main(int argc, char *argv[])
{
    // parameters
    // double density = 7.8e3; // kg / m^3
    // double lambda = 15;     // W / (m * K)
    // double specific_heat_capacity = 0.5e3;  // J / (kg * K)
    // double alpha = lambda / (density * specific_heat_capacity);
    double alpha = 110e-6; // m^2 / s  // 110e-6 = copper, 3.8e-6 = Edelstahl
    double L = 1;
    double T_max = 45;
    double target_x = 0.8;
    double T_start = 20;
    double h = L / N_POINTS;
    double tau = h*h / (2*alpha) * 0.1;
    double T_heating = 100;
    int i_iteration = 0;

    struct timeval t1, t2;
    int duration_us;

    int b_continue = 1;

    double u[N_POINTS];
    double u_new[N_POINTS];
    int i;
    double current_t = 0;
    int target_i = round(target_x / h);

    char *filename = "stick1.csv";
    FILE *output_csv_file;

    const double at = alpha * tau / (h*h);

    fprintf(stderr, "h = %lf, tau = %le, alpha = %lf mm^2/s %i\n", h, tau, alpha * 1e6, target_i);

    for(i = 1; i < N_POINTS; i++) {
        u[i] = T_start;
    }
    u[0] = T_heating;
    u_new[0] = T_heating;

    gettimeofday(&t1, NULL);

    #pragma omp parallel shared(u_new, u, i_iteration, current_t, b_continue)
    while(b_continue) {
        #pragma omp for
        for(i = 1; i < N_POINTS - 1; i++) {
            u_new[i] = u[i] + at * (u[i+1] + u[i-1] - 2*u[i]);
        }

        // parallelized version of memcpy. This resulted in a performance gain compared to running memcpy inside the omp single section
        #pragma omp for
        for(i = 1; i < N_POINTS - 1; i++) {
            u[i] = u_new[i];
        }

        // omp single: only executed by 1 thread
        // vs. omp critical: executed by all threads, but sequentially
        #pragma omp single
        {
            //u_new[N_POINTS - 1] = u_new[N_POINTS - 2];

            //memcpy(u, u_new, sizeof(double) * N_POINTS);  // overwrite u with the new values

            u[N_POINTS - 1] = u[N_POINTS - 2];

            i_iteration++;
            current_t = i_iteration * tau;

            if(i_iteration % 200000 == 0) {
                fprintf(stderr, "i_iteration=%7i, t=%9lf, u(0.8m)=%9lf, %9lf %9lf %9lf\r", i_iteration, current_t, u[target_i], u_new[N_POINTS - 3], u_new[N_POINTS - 2], u_new[N_POINTS - 1]);
                for(i = 0; i < N_POINTS; i++) {
                    printf("%d %lf\n", i, u[i]);
                }
            }

            if(u[target_i] > T_max) {
                b_continue = 0;
            }
        }
    }

    gettimeofday(&t2, NULL);
    duration_us = my_usecdiff(&t1, &t2);
    fprintf(stderr, "\nreached %lf *C on t = %lf s\n", u[target_i], current_t);
    fprintf(stderr, "computation took %lf seconds -> %lf us per iteration\n", duration_us / 1e6, duration_us / (double)i_iteration);

    return 0;
}