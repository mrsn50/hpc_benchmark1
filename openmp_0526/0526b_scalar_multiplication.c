/*
 * compile with: gcc -fopenmp -o 0526b 0526b.c 
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdint.h>

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

int main(int argc, char **argv) {
    double *a;
    double *b;
    double sum;
    double local_sum;
    int i;
    int N;
    struct timeval t1, t2;
    int duration_us;
    int M = 10;
    int i2;

    if(argc == 2) {
        N = atoi(argv[1]);
        printf("V2 N = %d\n", N);
    }
    else {
        printf("Usage: 0519a <N>\n");
        return -1;
    }

    a = malloc(N * sizeof(double));
    b = malloc(N * sizeof(double));

    for(i = 0; i < N; i++) {
        a[i] = 1 + i;
        b[i] = 1 + i;
    }

    // #pragma omp parallel for reduction (+ : sum)

    gettimeofday(&t1, NULL);
    for(i2 = 0; i2 < M; i2++) {
        sum = 0;
        #pragma omp parallel for reduction (+ : sum)
        for(i = 0; i < N; i++) {
            sum += a[i] * b[i];
        }
    }
    gettimeofday(&t2, NULL);
    duration_us = my_usecdiff(&t1, &t2);
    
    printf("scalar product: %lf\n", sum);
    printf("Operation took %d us -> effectively %ld MFLOPS\n", 
            duration_us / M, ((uint64_t)N * 2 - 1) * M / duration_us);

    return 0;
}