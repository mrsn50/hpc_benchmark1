import numpy as np
import matplotlib.pyplot as plt
import os
import sys

if (len(sys.argv) > 1) and (sys.argv[1] == 'exec'):
    script = """
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DUSE_ACCUMULATOR -DUSE_FUNCTION_FOR_MUL
    ./0428a_benchmark acc_mulfunc.csv
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DUSE_FUNCTION_FOR_MUL
    ./0428a_benchmark noacc_mulfunc.csv
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c
    ./0428a_benchmark noacc_nofunc_samem.csv
    """
    for line in script.split('\n'):
        print(line)
        if os.system(line) != 0:
            print("error running:", line)
            sys.exit(-1)

def benchmark(filename):
    # strip because '\n' on the end
    values = [[int(val) for val in line.strip().split(';')] for line in open(filename).readlines()]
    values = np.array(values)
    if values.shape[0] > 0:
        plt.plot(values[:,0], (values[:,1] * values[:,0] * values[:,0]) / values[:,2], '-x')
    plt.xlabel("N")
    plt.ylabel("ops / us")
    plt.xscale("log")
benchmark("acc_mulfunc.csv")
benchmark("noacc_mulfunc.csv")
benchmark("noacc_nofunc_samem.csv")
plt.legend(["acc + func", "no acc + func", "no func"])
plt.grid('both')
plt.show()