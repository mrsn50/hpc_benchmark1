/*
 * Runs a matrix vector multiplication of an NxN-matrix with a vector of size N on 4 cores using MPI.
 * This is done by letting each process compute the result for a part of the lines of the matrix.
 * Benchmarks this by using different N sizes.
 * M is kept constant across the benchmark.
 * 
 * This file is intended to be used in conjunction with 0505a_benchmark.py.
 * 
 * Compile with: mpicc -O2 -o 0505a_benchmark 0505a_benchmark.c
 * put -g after mpicc for debugging
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <mpi.h>

static int world_size;
static int world_rank;
static char processor_name[MPI_MAX_PROCESSOR_NAME];
static int name_len;

#define MY_TAG 5678
#define NUM_PASSES 10

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

// N: problem size, M: number of times the problem is repeated
// Returns the number of usec of the whole iteration (-> not divided by M)
int do_benchmark(int N, int M) {
    double *A;
    double *x;
    double *y;
    double *A_tmp;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;
    register double accumulator;
    int problem_iteration;

    if(N % world_size != 0) {
        printf("Error: N has to be a multiple of the processor count (%d) but is %d\n", world_size, N);
        return -1;
    }

    if(world_rank == 0) {
        // master process
        printf("N = %d, M = %d\n", N, M);

        A = malloc(sizeof(double) * N * N);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N);

        for(i = 0; i < N * N; i++) {
            A[i] = i + 1;
        }
        for(i = 0; i < N; i++) {
            x[i] = i + 1;
        }
    }
    else {
        A = malloc(sizeof(double) * N * N / world_size);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N / world_size);
    }

    // siehe Fixpunktiteration
    if(world_rank == 0) {
        for(i = 1; i < world_size; i++) {
            MPI_Send(A + N * N / world_size * i, 
                N * N / world_size, 
                MPI_DOUBLE, 
                i, 
                MY_TAG, 
                MPI_COMM_WORLD);
        }
        // the master process does not need the whole matrix anymore, so free it and keep only the first slice
        #ifndef DONT_FREE_MATRIX
        A_tmp = malloc(sizeof(double) * N * N / world_size);
        memcpy(A_tmp, A, sizeof(double) * N * N / world_size);
        free(A);
        A = A_tmp;
        #endif
    }
    else {
        MPI_Recv(A, 
            N * N / world_size, 
            MPI_DOUBLE, 
            0, 
            MY_TAG, 
            MPI_COMM_WORLD, 
            MPI_STATUS_IGNORE);
    }

    gettimeofday(&t1, NULL);
    for(problem_iteration = 0; problem_iteration < M; problem_iteration++) {
        // if(world_rank == 0) {
        //     for(i = 1; i < world_size; i++) {
        //         MPI_Send(x, N, MPI_DOUBLE, i, MY_TAG, MPI_COMM_WORLD);
        //     }
        // }
        // else {
        //     MPI_Recv(x, N, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        // }
        MPI_Bcast(x, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        for(irow = 0; irow < N / world_size; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        
        if(world_rank == 0) {
            for(i = 1; i < world_size; i++) {
                MPI_Recv(y + N / world_size * i, 
                    N / world_size, 
                    MPI_DOUBLE, 
                    i, 
                    MY_TAG, 
                    MPI_COMM_WORLD, 
                    MPI_STATUS_IGNORE);
            }
        }
        else {
            MPI_Send(y, N / world_size, MPI_DOUBLE, 0, MY_TAG, MPI_COMM_WORLD);
        }
    }
    gettimeofday(&t2, NULL);

    if(world_rank == 0) {
        duration_us = my_usecdiff(&t1, &t2);
        //printf("Operation took %d us incl. send and receive -> effectively %ld FLOPS / us\n", duration_us / M, (uint64_t)N * N * M / duration_us);

        if(N <= 4) {
            printf("y = ");
            for(i = 0; i < N; i++) {
                printf("%lf ", y[i]);
            }
            printf("\n");
        }
    }

    free(A);
    free(x);
    free(y);

    return duration_us;
}

int main(int argc, char *argv[]) {
    const char *filename;
    FILE *output_csv_file;
    double N_with_comma;
    int duration_us;
    int M;
    int N;
    int i;
    int num_passes;

    MPI_Init(&argc, &argv);
    // Get the number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    // Get the name of the processor
    MPI_Get_processor_name(processor_name, &name_len);

    if(argc == 2) {
        filename = argv[1];
    }
    else {
        printf("Usage: 0505a_benchamrk <output_csv_filename>\n");
        return -1;
    }

    if(world_rank == 0) {
        output_csv_file = fopen(filename, "w+");
        if(output_csv_file == NULL) {
            perror("Could not open output csv file");
            return -2;
        }
    }

    for(N_with_comma = world_size; N_with_comma < 10000; N_with_comma *= 1.4142135623730950488) {  // sqrt(2)
        N = ((int)N_with_comma / world_size) * world_size;  // N must be a multiple of world_size

        M = 500;
        if(N < 1000)
            num_passes = NUM_PASSES;
        else
            num_passes = 10;
        
        duration_us = 0;
        for(i = 0; i < num_passes; i++) {
            duration_us += do_benchmark(N, M);
            usleep(100000);  // cooldown, allow other processes to enter
        }

        if(world_rank == 0) {
            fprintf(output_csv_file, "%d;%d;%lf\n", N, M, (double)duration_us / num_passes);
        }
    }

    if(world_rank == 0) {
        fclose(output_csv_file);
    }

    MPI_Finalize();

    return 0;
}