/*
 * Runs a matrix vector multiplication of an NxN-matrix with a vector of size N on 4 cores using MPI.
 * This is done by letting each process compute the result for a part of the lines of the matrix.
 * Compile with: gcc -fopenmp -o 0526a 0526a.c 
 * to run with a limited number of threads, use taskset -c 1-8 ./0526a 2000 500
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <omp.h>

#define MY_TAG 5678

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

int main(int argc, char *argv[]) {
    double *A;
    double *x;
    double *y;
    double *A_tmp;
    int N;
    int M;
    int i;
    struct timeval t1, t2;
    int duration_us;
    int irow;
    int icolumn;
    register double accumulator;
    int problem_iteration;

    if(argc == 3) {
        N = atoi(argv[1]);
        M = atoi(argv[2]);
    }
    else {
        printf("Usage: 0526a <N> <M>\nN is the size of the matrix (NxN).\nM is the number of repetitions.\n");
        return -1;
    }

    // omp_get_num_threads returns the number of threads that are currently in use
    printf("N = %d, M = %d, max_threads = %d\n", N, M, omp_get_max_threads());

    A = malloc(sizeof(double) * N * N);
    x = malloc(sizeof(double) * N);
    y = malloc(sizeof(double) * N);

    for(i = 0; i < N * N; i++) {
        A[i] = i + 1;
    }
    for(i = 0; i < N; i++) {
        x[i] = i + 1;
    }

    gettimeofday(&t1, NULL);
    for(problem_iteration = 0; problem_iteration < M; problem_iteration++) {
        #ifdef INNER_LOOP
        // should be slower because of more overhead
        for(irow = 0; irow < N; irow++) {
            accumulator = 0;
            #pragma omp parallel for reduction (+ : accumulator)
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        #else
        #pragma omp parallel private(irow, accumulator, icolumn)
        #pragma omp for
        for(irow = 0; irow < N; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        #endif
    }
    gettimeofday(&t2, NULL);

    duration_us = my_usecdiff(&t1, &t2);
    printf("Operation took %d us incl. send and receive -> effectively %ld MFLOPS\n", 
            duration_us / M, (uint64_t)N * N * M / duration_us * 2);

    if(N <= 4) {
        printf("y = ");
        for(i = 0; i < N; i++) {
            printf("%lf ", y[i]);
        }
        printf("\n");
    }

    free(A);
    free(x);
    free(y);

    return 0;
}