# This will use the same number of operations (-> M) for all problem sizes

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

if (len(sys.argv) > 1) and (sys.argv[1] == 'exec'):
    script = """
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DSAME_M -DUSE_ACCUMULATOR -DUSE_FUNCTION_FOR_MUL
    ./0428a_benchmark acc_mulfunc_samem.csv
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DSAME_M -DUSE_FUNCTION_FOR_MUL
    ./0428a_benchmark noacc_mulfunc_samem.csv
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DSAME_M
    ./0428a_benchmark noacc_nofunc_samem.csv
    gcc -O2 -o 0428a_benchmark 0428a_benchmark.c -DSAME_M -DUSE_ACCUMULATOR
    ./0428a_benchmark acc_nofunc_samem.csv
    """
    for line in script.split('\n'):
        print(line)
        if os.system(line) != 0:
            print("error running:", line)
            sys.exit(-1)

def benchmark(filename):
    # strip because '\n' on the end
    values = [[float(val) for val in line.strip().split(';')] for line in open(filename).readlines()]
    values = np.array(values)
    if values.shape[0] > 0:
        plt.plot(values[:,0], (values[:,1] * values[:,0] * values[:,0]) / values[:,2], '-x')
    plt.xlabel("N")
    plt.ylabel("ops / us")
    plt.xscale("log")
benchmark("acc_mulfunc_samem.csv")
benchmark("noacc_mulfunc_samem.csv")
benchmark("noacc_nofunc_samem.csv")
benchmark("acc_nofunc_samem.csv")
plt.title("same M")
plt.legend(["acc + func", "no acc + func", "no func"])
plt.show()