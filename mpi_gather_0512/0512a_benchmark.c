/*
 * Runs a matrix vector multiplication of an NxN-matrix with a vector of size N on 4 cores using MPI.
 * This is done by letting each process compute the result for a part of the lines of the matrix.
 * Benchmarks this by using different N sizes.
 * M is kept constant across the benchmark.
 * 
 * This file is intended to be used in conjunction with 0512a_benchmark.py.
 * 
 * Compile with: mpicc -O2 -o 0512a_benchmark 0512a_benchmark.c
 * put -g after mpicc for debugging
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#include <mpi.h>

static int world_size;
static int world_rank;
static char processor_name[MPI_MAX_PROCESSOR_NAME];
static int name_len;

#define MY_TAG 5678
#define NUM_PASSES 10

int my_usecdiff(const struct timeval *start, const struct timeval *end) {
    return (end->tv_sec - start->tv_sec) * 1000000 + end->tv_usec - start->tv_usec;
}

// N: problem size, M: number of times the problem is repeated
// Returns the number of usec of the whole iteration (-> not divided by M)
int do_benchmark(int N, int M) {
    double *A;
    double *x;
    double *y;
    double *A_tmp;
    int i;
    struct timeval t1, t2;
    clock_t tc1, tc2;
    int duration_us = 0;
    int duration_clockt_us = 0;
    int irow;
    int icolumn;
    register double accumulator;
    int problem_iteration;
    int *root_recvcounts;
    int *root_displacements;

    if(N % world_size != 0) {
        printf("Error: N has to be a multiple of the processor count (%d) but is %d\n", world_size, N);
        return -1;
    }

    if(world_rank == 0) {
        // master process
        printf("N = %d, M = %d", N, M);

        A = malloc(sizeof(double) * N * N);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N);

        for(i = 0; i < N * N; i++) {
            A[i] = i + 1;
        }
        for(i = 0; i < N; i++) {
            x[i] = i + 1;
        }
    }
    else {
        A = malloc(sizeof(double) * N * N / world_size);
        x = malloc(sizeof(double) * N);
        y = malloc(sizeof(double) * N);
    }

    // siehe Fixpunktiteration -> vor der Zeitmessung
    MPI_Scatter(A, N * N / world_size, MPI_DOUBLE, A, N * N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    #ifndef DONT_FREE_MATRIX
    if(world_rank == 0) {
        // the master process does not need the whole matrix anymore, so free it and keep only the first slice
        A_tmp = malloc(sizeof(double) * N * N / world_size);
        memcpy(A_tmp, A, sizeof(double) * N * N / world_size);
        free(A);
        A = A_tmp;
    }
    #endif

    if(world_rank == 0) {
        root_recvcounts = malloc(sizeof(int) * world_size);
        root_displacements = malloc(sizeof(int) * world_size);
        root_recvcounts[0] = 0;  // the master process won't receive anything from itself
        root_displacements[0] = 0;
        for(i = 1; i < world_size; i++) {
            root_recvcounts[i] = N / world_size;
            root_displacements[i] = N / world_size * i;
        }
    }
    
    // sleep(1);  // Wait to complete memory allocation. Usleep had the very strange problem that on mat-dell-notebook, the calculation took much longer.

    MPI_Barrier(MPI_COMM_WORLD);

    tc1 = clock();
    gettimeofday(&t1, NULL);
    for(problem_iteration = 0; problem_iteration < M; problem_iteration++) {
        MPI_Bcast(x, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        for(irow = 0; irow < N / world_size; irow++) {
            accumulator = 0;
            for(icolumn = 0; icolumn < N; icolumn++) {
                accumulator += A[irow*N + icolumn] * x[icolumn];
            }
            y[irow] = accumulator;
        }
        
        #if defined(GATHER)
        #if defined(GATHER_V)
        MPI_Gatherv(y, N / world_size, MPI_DOUBLE, y, root_recvcounts, root_displacements, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        #elif defined(DGATHER_INPLACE)
        if(world_rank == 0)
            MPI_Gather(MPI_IN_PLACE, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        else
            MPI_Gather(y, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        #else
        MPI_Gather(y, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        #endif
        #elif defined(ALLGATHER)
        MPI_Allgather(y, N / world_size, MPI_DOUBLE, y, N / world_size, MPI_DOUBLE, MPI_COMM_WORLD);
        #endif
    }
    gettimeofday(&t2, NULL);
    tc2 = clock();

    if(world_rank == 0) {
        duration_us = my_usecdiff(&t1, &t2);
        duration_clockt_us = tc2 - tc1;
        printf(" %d %d\n", duration_us, duration_clockt_us);
        //printf("Operation took %d us incl. send and receive -> effectively %ld FLOPS / us\n", duration_us / M, (uint64_t)N * N * M / duration_us);

        if(N <= 4) {
            printf("y = ");
            for(i = 0; i < N; i++) {
                printf("%lf ", y[i]);
            }
            printf("\n");
        }
    }

    if(world_rank == 0) {
        free(root_recvcounts);
        free(root_displacements);
    }

    free(A);
    free(x);
    free(y);

    return duration_us;
}

int main(int argc, char *argv[]) {
    const char *filename;
    FILE *output_csv_file;
    double N_with_comma;
    uint64_t duration_us;
    int M;
    int N;
    int i;
    int num_passes;

    MPI_Init(&argc, &argv);
    // Get the number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    // Get the name of the processor
    MPI_Get_processor_name(processor_name, &name_len);

    //do_benchmark(724, 500);
    //MPI_Finalize();
    //return 0;

    if(argc == 2) {
        filename = argv[1];
    }
    else {
        printf("Usage: 0505a_benchamrk <output_csv_filename>\n");
        return -1;
    }

    if(world_rank == 0) {
        printf("sizeof(int) = %ld, sizeof(void*) = %ld, CLOCKS_PER_SEC = %ld\n", sizeof(int), sizeof(void*), CLOCKS_PER_SEC);
        output_csv_file = fopen(filename, "w+");
        if(output_csv_file == NULL) {
            perror("Could not open output csv file");
            return -2;
        }
    }

    for(N_with_comma = world_size; N_with_comma < 10000; N_with_comma *= 1.4142135623730950488) {  // sqrt(2)
        N = ((int)N_with_comma / world_size) * world_size;  // N must be a multiple of world_size

        M = 500;
        if(N < 1000)
            num_passes = NUM_PASSES;
        else
            num_passes = 10;
        
        duration_us = 0;
        for(i = 0; i < num_passes; i++) {
            duration_us += do_benchmark(N, M);
            //usleep(100000);  // cooldown, allow other processes to enter
        }

        if(world_rank == 0) {
            fprintf(output_csv_file, "%d;%d;%lf\n", N, M, (double)duration_us / num_passes);
        }
    }

    if(world_rank == 0) {
        fclose(output_csv_file);
    }

    MPI_Finalize();

    return 0;
}
